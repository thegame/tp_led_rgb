import tkinter as tk
import serial

#ser = serial.Serial('COM4', timeout=1)
ser = serial.Serial('/dev/ttyUSB0', 9600)

class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        tk.Label(self, text="Rouge").grid(row=0, column=0)
        tk.Label(self, text="Vert").grid(row=1, column=0)
        tk.Label(self, text="Bleu").grid(row=2, column=0)
        self.scale_rouge = tk.Scale(self, from_=0, to=9)
        self.scale_rouge.grid(column=1, row=0)
        self.scale_vert = tk.Scale(self, from_=0, to=9)
        self.scale_vert.grid(column=1, row=1)
        self.scale_bleu = tk.Scale(self, from_=0, to=9)
        self.scale_bleu.grid(column=1, row=2)
        tk.Button(self, text='Push', command=self.send_data).grid(column=2, row=1)

    def send_data(self):
        rouge = self.scale_rouge.get()
        vert = self.scale_vert.get()
        bleu = self.scale_bleu.get()
        ser.write((str(rouge) + str(vert) + str(bleu)).encode('utf-8'))

    
        
root = tk.Tk()
app = Application(master=root)
app.master.title("Led controller")
app.mainloop()
